package recipes.vaadin.recipe4;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.WrappedHttpSession;
import com.vaadin.flow.server.WrappedSession;
import org.springframework.beans.factory.annotation.Autowired;

@Route("")
public class LogoutView extends VerticalLayout {

    public LogoutView() {
        add(new Button("Log me out", e -> {
            getUI().ifPresent(ui -> {
                ui.getPage().executeJavaScript("window.location.href = 'https://vaadin.recipes/'");
                final WrappedHttpSession session = (WrappedHttpSession) VaadinSession.getCurrent().getSession();
                session.getHttpSession().invalidate();
            });
        }));
    }

}