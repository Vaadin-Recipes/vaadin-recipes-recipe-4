package recipes.vaadin.recipe4;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.spring.annotation.SpringComponent;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@SpringComponent
@Slf4j
public class SessionCountGaugeServiceInitListener implements VaadinServiceInitListener {

    private final MeterRegistry meterRegistry;

    public SessionCountGaugeServiceInitListener(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public void serviceInit(ServiceInitEvent event) {

        final AtomicInteger sessionsCount = meterRegistry.gauge("vaadin.sessions", new AtomicInteger(0));

        final VaadinService vaadinService = event.getSource();

        vaadinService.addSessionInitListener(e -> {
            log.info("New Vaadin session created. Current count is: " + sessionsCount.incrementAndGet());
        });
        vaadinService.addSessionDestroyListener(e -> {
            log.info("Vaadin session destroyed. Current count is: " + sessionsCount.decrementAndGet());
        });
    }
}
